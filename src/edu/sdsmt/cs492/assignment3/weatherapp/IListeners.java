package edu.sdsmt.cs492.assignment3.weatherapp;

import edu.sdsmt.cs492.assignment3.weatherapp.model.Forecast;
import edu.sdsmt.cs492.assignment3.weatherapp.model.ForecastLocation;

/*********************************************************
* interface IListeners
* contains the interface used by the main activity
*  
*  @author Victor Dozal, Dan Halloran
********************************************************/
public interface IListeners{
	public void onLocationLoaded(ForecastLocation forecastLocation);
	public void onForecastLoaded(Forecast forecast);
}
