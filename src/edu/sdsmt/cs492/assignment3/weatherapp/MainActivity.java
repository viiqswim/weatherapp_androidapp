/**********************************************************
 * Title:
 * 		Weather App
 * Authors:
 * 		Victor Dozal, Dan Halloran
 * Class:
 * 		CSC 492, Mobile Computing
 * Instructors:
 * 		Dr. Logar
 * 		Professor Brian Butterfield
 * Date:
 * 		November 18th, 2013
 * Description:
 * 		This app allows users to view the current weather
 * 		in Rapid City, SD (57701)
 
 * Program Errors:
 * 		none
 * 
 *********************************************************/

package edu.sdsmt.cs492.assignment3.weatherapp;

import android.app.Activity;
import android.net.NetworkInfo;
import android.content.Context;
import android.os.Bundle;
import android.net.ConnectivityManager;
import android.app.FragmentManager;
import android.widget.Toast;
import edu.sdsmt.cs492.assignment3.weatherapp.model.Forecast;
import edu.sdsmt.cs492.assignment3.weatherapp.model.Forecast.LoadForecast;
import edu.sdsmt.cs492.assignment3.weatherapp.model.ForecastLocation;
import edu.sdsmt.cs492.assignment3.weatherapp.model.ForecastLocation.LoadForecastLocation;
import edu.sdsmt.cs492.assignment3.weatherapp.view.FragmentForecast;

/*********************************************************
*
*  Contains the functions for the main flow of the app.
*	Extends:
*		Activity
*  Implements:
*  	IListeners
*  @author Victor Dozal, Dan Halloran
********************************************************/
public class MainActivity extends Activity implements IListeners
{
	public final static String KEY_FORECAST = "Forecast";
	public final static String KEY_WEATHER_BUNDLE = "WeatherBundle";
	private final static String FRAGMENT_FORECAST_TAG = "Fragment_List";
	
	private FragmentManager _fragmentManager;
	private FragmentForecast _fragmentForecast;

	private  ForecastLocation _forecastLocation;
	private  Forecast _forecast;
	
	private LoadForecastLocation loadForecastLocation; 
	private LoadForecast loadForecast;
	
	/************************************************************************
 	 * 	onCreate()
 	 * 	Sets the main activity view and initializes the fragment manager
 	 *  @author Victor Dozal, Dan Halloran
 	 * 	@param Bundle savedInstanceState
 	 *  @return Nothing
 	 ***********************************************************************/
	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
	
		// Sets the content of the Main Activity to activity_main.xml
		setContentView(R.layout.activity_main);
		 _forecastLocation = new ForecastLocation();
		 _forecast = new Forecast();

		// Get an instance of fragment manager
		_fragmentManager = getFragmentManager();

		// Add/Replace list fragment if the bundle is empty 
		if (savedInstanceState == null) {
			_fragmentForecast = new FragmentForecast();
			_fragmentManager.beginTransaction().replace(
													R.id.ContainerFrame, 
													_fragmentForecast,
													FRAGMENT_FORECAST_TAG)
											   .commit();
		} 
		// otherwise activity is being re-created, so keep the fragment 
		// that is already displayed.
		else {
			_fragmentForecast = (FragmentForecast) _fragmentManager.findFragmentByTag(FRAGMENT_FORECAST_TAG);			
		}
	}
	
	/************************************************************************
 	 * 	onResume()
 	 * 	Updates the loading bar, the forecast, and the forecast location
 	 *  @author Victor Dozal, Dan Halloran
 	 * 	@param Nothing
 	 *  @return Nothing
 	 ***********************************************************************/
	@Override
	protected void onResume() {
		super.onResume();
		
		// Update the loading bar
		getCorrectLoadBar();
		
		// Gets a Forecast Loader
		loadForecast = _forecast.new LoadForecast(this, this);
		loadForecast.execute(getResources().getString(R.string.city));
		
		// Gets a Forecast Location Loader
		loadForecastLocation = _forecastLocation.new LoadForecastLocation(this, this);
		loadForecastLocation.execute(getResources().getString(R.string.city));
		
		// If the forecast loader is not null
		if(_forecast != null && _forecastLocation != null){
			// Update the forecast
			_fragmentForecast.updateWeather(_forecast);
			// Update the location
			_fragmentForecast.getLocation(_forecastLocation);
		}
	}
	
	/************************************************************************
 	 * 	onPause()
 	 * 	Cancels loading the current forecast
 	 *  @author Victor Dozal, Dan Halloran
 	 * 	@param Nothing
 	 *  @return Nothing
 	 ***********************************************************************/
	@Override
	protected void onPause() {
		super.onPause();
		// Cancel the load of a Forecast
		loadForecastLocation.cancel(true);
		loadForecast.cancel(true);		
	}
	
	/************************************************************************
 	 * 	onSaveInstanceState()
 	 * 	Saves the parcelable data for forecast and forecast location
 	 *  @author Victor Dozal, Dan Halloran
 	 * 	@param Bundle state
 	 *  @return Nothing
 	 ***********************************************************************/
	@Override
	protected void onSaveInstanceState(Bundle state){
		super.onSaveInstanceState(state);
		state.putParcelable(Forecast.KEY_FORECAST, _forecast);
		state.putParcelable(ForecastLocation.KEY_LOCATION, _forecastLocation);
	}

	/************************************************************************
 	 * 	onForecastLoaded()
 	 * 	Sets the current forecast and updates the fragment and loadbar
 	 *  @author Victor Dozal, Dan Halloran
 	 * 	@param Forecast forecast
 	 *  @return Nothing
 	 ***********************************************************************/
	@Override
	public void onForecastLoaded(Forecast forecast){
		_forecast = forecast;
		_fragmentForecast.updateWeather(forecast);
		getCorrectLoadBar();
	}	
	
	/************************************************************************
 	 * 	onLoationLoaded()
 	 * 	Sets the current location and updates fragment and loadbar
 	 *  @author Victor Dozal, Dan Halloran
 	 * 	@param ForecastLocation forecastLocation
 	 *  @return Nothing
 	 ***********************************************************************/
	@Override
	public void onLocationLoaded(ForecastLocation forecastLocation){
		_fragmentForecast.getLocation(forecastLocation);
		_forecastLocation = forecastLocation;
		getCorrectLoadBar();
	}
	
	/************************************************************************
 	 * 	getCorrectLoadBar()
 	 * 	Checks for if the forecast is loaded and if not, it displays the
 	 *  appropriate message
 	 *  @author Victor Dozal, Dan Halloran
 	 * 	@param Nothing
 	 *  @return Nothing
 	 ***********************************************************************/
	public void getCorrectLoadBar(){
		// Gets the message for the progress bar
		String message = getResources().getString(R.string.progressMessage);		
		
		// Checks if the UI should load
		Boolean willLoad = false;
		
		// If the forecast location is not null and it is empty
		if(_forecastLocation != null){
			if(_forecastLocation.equals(new ForecastLocation())){
				willLoad = true;
			}
		}
		
		// If the forecast is not null and it is empty
		if(_forecast != null){
			if(_forecast.equals(new Forecast())){
				willLoad = true;
			}
		}
		
		// Update the load bar with the appropriate message
		_fragmentForecast.getCorrectLoadBar(willLoad, message);	
		
		getConnectionStatus();
	}
	
	/************************************************************************
 	 * 	getConnectionStatus()
 	 * 	Displays a toast and textbox if there's no network connctivity
 	 *  @author Victor Dozal, Dan Halloran
 	 * 	@param Nothing
 	 *  @return Nothing
 	 ***********************************************************************/
	public void getConnectionStatus(){
		// Get a connect manager
		ConnectivityManager connectManager = (ConnectivityManager) getSystemService(Context.CONNECTIVITY_SERVICE);
		
		// Get the network information
		NetworkInfo networkInfo = connectManager.getActiveNetworkInfo();
		
		// If the network information is not empty, and it is connected
		if (networkInfo == null) {
			// Display that there is no internet connection
			Toast.makeText(getApplicationContext(), "You are not connected to the internet", Toast.LENGTH_SHORT).show();
			// Tell the user how to refresh their connectivity
			_fragmentForecast.getCorrectLoadBar(true, "No internet, rotate to refresh");		
		}	
	}
}
