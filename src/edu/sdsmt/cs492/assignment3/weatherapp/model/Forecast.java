package edu.sdsmt.cs492.assignment3.weatherapp.model;

import java.io.BufferedReader;
import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.UnsupportedEncodingException;
import java.net.MalformedURLException;
import java.net.URL;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.Locale;
import java.util.TimeZone;

import org.apache.http.HttpEntity;
import org.apache.http.HttpResponse;
import org.apache.http.client.ClientProtocolException;
import org.apache.http.client.methods.HttpGet;
import org.apache.http.impl.client.DefaultHttpClient;
import org.json.JSONException;
import org.json.JSONObject;

import android.content.Context;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.os.AsyncTask;
import android.os.Parcel;
import android.os.Parcelable;
import android.util.Log;

import edu.sdsmt.cs492.assignment3.weatherapp.IListeners;
import edu.sdsmt.cs492.assignment3.weatherapp.R;

/*********************************************************
*
*  Class that initializes the forecast data and the url
*  string. Also sets the data from a json object.
*	implements:
*		Parcelable
*  @author Victor Dozal, Dan Halloran
********************************************************/
public class Forecast implements Parcelable
{
	private static final String TAG = "Forecast";
	public static final String KEY_FORECAST = "key_forecast";

	Date convertedDate;
	
	public String _Temp;
	public String _Humidity;
	public String _PrecipChance;
	public String _Feels;
	public byte[] _bitmap;
	public String _Time;
	

	private static String _URL = "http://i.wxbug.net/REST/Direct/GetForecastHourly.ashx?zip=" + "%s" + 
	                      "&ht=t&ht=i&ht=cp&ht=fl&ht=h" + 
	                      "&api_key=ws69ryhcmf2yj8u2bfa9r84h";
	
	private String _imageURL = "http://img.weather.weatherbug.com/forecast/icons/localized/500x420/en/trans/%s.png";

	/*********************************************************
	 *  Forecast()
	 *  Initializes all the forecast data to null.
     *  @author Victor Dozal, Dan Halloran
	 * 	@param Nothing
	 *  @return Nothing
	 ********************************************************/
	public Forecast(){
		// Initializes fields to null
		_Time = null;
		_Temp = null;
		_PrecipChance = null;
		_Feels = null;
		_Humidity = null;
	}
	
	/*********************************************************
	 *  Forecast()
	 *  Initializes all the forecast data from the parcel array
     *  @author Victor Dozal, Dan Halloran
	 * 	@param Parcel parc
	 *  @return Nothing
	 ********************************************************/
	public Forecast(Parcel parc) {
		String[] pArray = new String[10];
		parc.readStringArray(pArray);

		_Humidity = pArray[0];
		_Temp = pArray[1];
		_PrecipChance = pArray[2];
		_Time = pArray[3];
		_Feels = pArray[4];
	}
	
	/*********************************************************
	 *  Forecast()
	 *  Attempts to parse the json object for forecast data
     *  @author Victor Dozal, Dan Halloran
	 * 	@param JSONObject input
	 *  @return Nothing
	 ********************************************************/
	public Forecast(JSONObject input) {
		JSONObject obj = new JSONObject();
		if (input == null)
			return;
	
		// Parses the JSONObject
		try {
			obj = (JSONObject) input.getJSONArray("forecastHourlyList").get(0);
			
			_Temp = obj.getString("temperature");
			_Humidity = obj.getString("humidity");
			_Time = obj.getString("dateTime");
			_PrecipChance = obj.getString("chancePrecip");
			_Feels = obj.getString("feelsLike");
			_bitmap = convertToByteArray(readImg(obj.getString("icon"), -1));
			
			// Parses the time
			convertedDate = new Date(Long.parseLong(_Time));
			DateFormat dateFormat = new SimpleDateFormat("h:00 a", Locale.US);
			// Sets the timezone
			dateFormat.setTimeZone(TimeZone.getTimeZone("GMT"));
			// Stores formatted time
			_Time = dateFormat.format(convertedDate);			

		} catch (JSONException e) {
			Log.e(TAG, e.toString());
		}
	}
	
	/*********************************************************
	 *  equals()
	 *  Checks if current time and temp matches what we have.
     *  @author Victor Dozal, Dan Halloran
	 * 	@param Object obj
	 *  @return Nothing
	 ********************************************************/
    @Override
	public boolean equals(Object obj) {
		Forecast input = (Forecast) obj;
		
		if(_Time == null || input._Time == null){
			if(_Time != input._Time)
				return false;
		}
		else if(!input._Time.equals(_Time)){
			return input._Time.equals(_Time);
		}
		
		if(_Temp == null || input._Temp == null){
			if(_Temp != input._Temp)
				return false;
		}
		else if(!input._Temp.equals(_Temp)){
			return input._Temp.equals(_Temp);
		}
		
		return true;
	}
	
	/*********************************************************
	 *  getURL()
	 *  Formats the URL string
     *  @author Victor Dozal, Dan Halloran
	 * 	@param String ZipCode, Context context
	 *  @return Formatted string
	 ********************************************************/
	public static String getURL(String ZipCode, Context context) {
		return String.format(_URL, 
						 	 ZipCode, 
						 	 context.getResources()
						 	 		.getString(R.string.Something));
	}

	/*********************************************************
	 *  describeContents()
	 *  Not used, but neccesary for program to run
     *  @author Victor Dozal, Dan Halloran
	 * 	@param Nothing
	 *  @return Nothing
	 ********************************************************/
	@Override
	public int describeContents(){
		return 0;
	}
    
	/*********************************************************
	 *  writeToParcel()
	 *  Writes the forecast data to the parcel array.
     *  @author Victor Dozal, Dan Halloran
	 * 	@param Parcel dest, int flags
	 *  @return Nothing
	 ********************************************************/
	@Override
	public void writeToParcel(Parcel dest, int flags) {
		String[] parcelArray = new String[10];
		
		// Writes the data to an array
		parcelArray[0] = _Humidity;
		parcelArray[1] = _Temp;
		parcelArray[2] = _PrecipChance;
		parcelArray[3] = _Time;
		parcelArray[4] = _Feels;
		
		dest.writeArray(parcelArray);
	}
	
	/*********************************************************
	 *  convertToByteArray()
	 *  Compresses image and stores in a byte array
     *  @author Victor Dozal, Dan Halloran
	 * 	@param Bitmap img
	 *  @return byteArray
	 ********************************************************/
	private byte[] convertToByteArray(Bitmap img) {
		// Initializes byte array stream
		ByteArrayOutputStream arrayStream = new ByteArrayOutputStream();
		// Compresses the image and puts it in the arrayStream
		img.compress(Bitmap.CompressFormat.PNG, 100, arrayStream);
		// Turns the arrayStream into a byte array
		byte[] byteArray = arrayStream.toByteArray();

		try {
			arrayStream.close();
		} catch (IOException e) {
			Log.e(TAG, e.toString());
		}
			
		arrayStream = null;

		return byteArray;
	}

	/*********************************************************
	 *  readImg()
	 *  Reads in appropriate image from the weather url
     *  @author Victor Dozal, Dan Halloran
	 * 	@param String condition, int size
	 *  @return iconBitmap
	 ********************************************************/
	private Bitmap readImg(String condition, int size) {
		Bitmap iconBitmap = null;
		try {
			BitmapFactory.Options options = new BitmapFactory.Options();
			if (size != -1) {
				options.inSampleSize = size;
			}
			
			URL weatherURL = new URL(String.format(_imageURL, condition));
			iconBitmap = BitmapFactory.decodeStream(weatherURL.openStream(), null, options);
			
		} catch (MalformedURLException e) {
			Log.e(TAG, e.toString());
		} catch (IOException e) {
			Log.e(TAG, e.toString());
		} catch (Exception e) {
			Log.e(TAG, e.toString());
		}

		return iconBitmap;
	}
	
	/*********************************************************
	 *  getJSONFromURL()
	 *  Takes in a URL, makes the API call, and returns the
	 *  JSONObject
     *  @author Victor Dozal, Dan Halloran
	 * 	@param String URL
	 *  @return JSONObject
	 ********************************************************/
	public static JSONObject getJSONFromUrl(String URL) {

		// Make HTTP Request (aka API call)
		InputStream inputStream = makeHTTPRequest(URL);
		
		// return JSON String
		return getJSONObj(inputStream);
	}
	
	/*********************************************************
	 *  makeHTTPRequest()
	 *  Attempts to connect to the http stream
     *  @author Victor Dozal, Dan Halloran
	 * 	@param String URL
	 *  @return inputStream
	 ********************************************************/
	private static InputStream makeHTTPRequest(String URL){

		InputStream inputStream = null;
		
		// Making HTTP request
		try {
			// defaultHttpClient
			DefaultHttpClient httpClient = new DefaultHttpClient();

			HttpGet httpPost = new HttpGet(URL);
			HttpResponse httpResponse = httpClient.execute(httpPost);
			HttpEntity httpEntity = httpResponse.getEntity();
			inputStream = httpEntity.getContent();

		} catch (UnsupportedEncodingException e) {
			e.printStackTrace();
		} catch (ClientProtocolException e) {
			e.printStackTrace();
		} catch (IOException e) {
			e.printStackTrace();
		}
		
		return inputStream;
	}
	
	/*********************************************************
	 *  getJSONObj()
	 *  attempts to read in json string from input stream
     *  @author Victor Dozal, Dan Halloran
	 * 	@param InputStream inputstream
	 *  @return jObj
	 ********************************************************/
	private static JSONObject getJSONObj(InputStream inputStream){
		JSONObject jObj = null;
		String json = "";
		
		try {
			StringBuilder builder = new StringBuilder();
			BufferedReader reader = new BufferedReader(new InputStreamReader(inputStream, "iso-8859-1"), 8);
			String line = null;
			while ((line = reader.readLine()) != null) {
				builder.append(line + "\n");
			}
			inputStream.close();
			json = builder.toString();
		} catch (Exception e) {
			Log.e("Buffer Error", "Error converting result " + e.toString());
		}

		// try parse the string to a JSON object
		try {
			if(!json.isEmpty()){
				jObj = new JSONObject(json);
			}
		} catch (JSONException e) {
			Log.e("JSON Parser", "Error parsing data " + e.toString());
		}

		// return JSON String
		return jObj;
	}

	/*********************************************************
	*
	*  Class that gets the forecast data asynchronously
	*  string. Also sets the data from a json object.
	*	extends:
	*		AsyncTask
	*  @author Victor Dozal, Dan Halloran
	********************************************************/
	public class LoadForecast extends AsyncTask<String, Void, Forecast>{
		private Context _context;
		private IListeners _listener;
		
		public LoadForecast(Context context, IListeners listener) {
			_context = context;
			_listener = listener;
		}

		@Override
		protected Forecast doInBackground(String ... parameters) {
			Forecast forecast = new Forecast();

			JSONObject weather = getJSONFromUrl(getURL(parameters[0], _context));
			forecast = new Forecast(weather);

			return forecast;
		}

		@Override
		protected void onPostExecute(Forecast forecast) {
			_listener.onForecastLoaded(forecast);
		}
	}
}
