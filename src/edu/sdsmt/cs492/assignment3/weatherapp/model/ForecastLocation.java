package edu.sdsmt.cs492.assignment3.weatherapp.model;

import org.apache.http.HttpEntity;
import org.apache.http.HttpResponse;
import org.apache.http.client.ClientProtocolException;
import org.apache.http.client.methods.HttpGet;
import org.apache.http.impl.client.DefaultHttpClient;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.UnsupportedEncodingException;

import android.os.Parcel;
import android.os.Parcelable;
import android.os.AsyncTask;
import android.util.Log;
import android.content.Context;

import edu.sdsmt.cs492.assignment3.weatherapp.IListeners;
import edu.sdsmt.cs492.assignment3.weatherapp.R;

/*********************************************************
*
*  Class that initializes all the location data
*  Implements:
*  	Parcelable
*  @author Victor Dozal, Dan Halloran
********************************************************/
public class ForecastLocation implements Parcelable
{
	private static final String TAG = "ForecastLocation";
	public static final String KEY_LOCATION = "key_location";
	public String _State;
	public String _ZipCode;
	public String _Country;
	public String _City;
	
	private static String _URL = "http://i.wxbug.net/REST/Direct/GetLocation.ashx?zip=" + "%s" + 
			             "&api_key=ws69ryhcmf2yj8u2bfa9r84h";

	/*********************************************************
	 *  ForecastLocation()
	 *  Initializes all the forecast location data to null.
     *  @author Victor Dozal, Dan Halloran
	 * 	@param Nothing
	 *  @return Nothing
	 ********************************************************/
	public ForecastLocation() {
		_State = null;
		_ZipCode = null;
		_Country = null;
		_City = null;
	}

	/*********************************************************
	 *  ForecastLocation()
	 *  Initializes all the forecast location data from the
	 *  parcel array
     *  @author Victor Dozal, Dan Halloran
	 * 	@param Parcel parcel
	 *  @return Nothing
	 ********************************************************/
	public ForecastLocation(Parcel parcel) {
		String[] parcelArray = new String[50];
		parcel.readStringArray(parcelArray);
		
		_ZipCode = parcelArray[0];
		_City = parcelArray[1];
		_State = parcelArray[2];
		_Country = parcelArray[3];
	}

	/*********************************************************
	 *  Forecast()
	 *  Attempts to parse the json object for location data
     *  @author Victor Dozal, Dan Halloran
	 * 	@param JSONObject jObj
	 *  @return Nothing
	 ********************************************************/
	public ForecastLocation(JSONObject jObj) {
		if (jObj == null)
			return;

		try {
			JSONObject obj = jObj.getJSONObject("location");
			_State = obj.getString("state");
			_ZipCode = obj.getString("zipCode");
			_Country = obj.getString("country");
			_City = obj.getString("city");
		} catch (JSONException e) {
			Log.e(TAG, e.toString());
		}
	}

	/*********************************************************
	 *  equals()
	 *  Checks if current zip code and city matches what we have.
     *  @author Victor Dozal, Dan Halloran
	 * 	@param Object obj
	 *  @return Nothing
	 ********************************************************/
	@Override
	public boolean equals(Object obj) {
		ForecastLocation input = (ForecastLocation) obj;
		
		if(_ZipCode == null || input._ZipCode == null){
			if(_ZipCode != input._ZipCode)
				return false;
		}
		else if(!input._ZipCode.equals(_ZipCode)){
			return input._ZipCode.equals(_ZipCode);
		}
		
		if(_City == null || input._City == null){
			if(_City != input._City)
				return false;
		}
		else if(!input._City.equals(_City)){
			return input._City.equals(_City);
		}
		 
		return true;
	}

	/*********************************************************
	 *  describeContents()
	 *  Not used, but neccesary for program to run
     *  @author Victor Dozal, Dan Halloran
	 * 	@param Nothing
	 *  @return Nothing
	 ********************************************************/
	@Override
	public int describeContents() {
		return 0;
	}
	
	/*********************************************************
	 *  writeToParcel()
	 *  Writes the location data to the parcel array.
     *  @author Victor Dozal, Dan Halloran
	 * 	@param Parcel dest, int flags
	 *  @return Nothing
	 ********************************************************/
	@Override
	public void writeToParcel(Parcel dest, int flags) {
		String[] parcelArray = new String[10];
		parcelArray[0] = _ZipCode;
		parcelArray[1] = _City;
		parcelArray[2] = _State;
		parcelArray[3] = _Country;
		dest.writeArray(parcelArray);
	}

	/*********************************************************
	 *  getJSONFromURL()
	 *  Takes in a URL, makes the API call, and returns the
	 *  JSONObject
     *  @author Victor Dozal, Dan Halloran
	 * 	@param String URL
	 *  @return JSONObject
	 ********************************************************/
	public static JSONObject getJSONFromUrl(String URL) {

		// Makes the HTTP request (aka API call)
		InputStream inputStream = makeHTTPRequest(URL);

		// Gets the json object from the API call and returns it
		return getJSONObj(inputStream);
	}
	
	/*********************************************************
	 *  makeHTTPRequest()
	 *  Attempts to connect to the http stream
     *  @author Victor Dozal, Dan Halloran
	 * 	@param String URL
	 *  @return inputStream
	 ********************************************************/
	private static InputStream makeHTTPRequest(String URL){

		InputStream inputStream = null;
		
		// Making HTTP request
		try {
			// defaultHttpClient
			DefaultHttpClient httpClient = new DefaultHttpClient();

			HttpGet httpPost = new HttpGet(URL);
			HttpResponse httpResponse = httpClient.execute(httpPost);
			HttpEntity httpEntity = httpResponse.getEntity();
			inputStream = httpEntity.getContent();

		} catch (UnsupportedEncodingException e) {
			e.printStackTrace();
		} catch (ClientProtocolException e) {
			e.printStackTrace();
		} catch (IOException e) {
			e.printStackTrace();
		}
		
		return inputStream;
	}
	
	/*********************************************************
	 *  getJSONObj()
	 *  attempts to read in json string from input stream
     *  @author Victor Dozal, Dan Halloran
	 * 	@param InputStream inputstream
	 *  @return jObj
	 ********************************************************/
	private static JSONObject getJSONObj(InputStream inputStream){
		JSONObject jObj = null;
		String json = "";
		
		try {
			BufferedReader reader = new BufferedReader(new InputStreamReader(
					inputStream, "iso-8859-1"), 8);
			StringBuilder sb = new StringBuilder();
			String line = null;
			while ((line = reader.readLine()) != null) {
				sb.append(line + "\n");
			}
			inputStream.close();
			json = sb.toString();
		} catch (Exception e) {
			Log.e("Buffer Error", "Error converting result " + e.toString());
		}

		// try parse the string to a JSON object
		try {
			jObj = new JSONObject(json);
		} catch (JSONException e) {
			Log.e("JSON Parser", "Error parsing data " + e.toString());
		}

		// return JSON String
		return jObj;
	}
	
	/*********************************************************
	*
	*  Class that gets the forecast data asynchronously
	*  string. Also sets the data from a json object.
	*	extends:
	*		AsyncTask
	*  @author Victor Dozal, Dan Halloran
	********************************************************/
	public class LoadForecastLocation extends AsyncTask<String, Void, ForecastLocation> {
		private Context _context;
		private IListeners _listener;

		public LoadForecastLocation(Context context, IListeners listener) {
			_context = context;
			_listener = listener;
		}

		@Override
		protected ForecastLocation doInBackground(String... params) {
			ForecastLocation location = new ForecastLocation();

			String zipcode = params[0];
			String str = String.format(_URL, zipcode, _context.getResources().getString(R.string.Something));
			
			JSONObject locationJSON = getJSONFromUrl(str);
			location = new ForecastLocation(locationJSON);

			return location;
		}

		@Override
		protected void onPostExecute(ForecastLocation location) {
			_listener.onLocationLoaded(location);
		}
	}
}
