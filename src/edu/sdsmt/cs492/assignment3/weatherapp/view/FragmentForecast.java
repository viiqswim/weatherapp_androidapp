package edu.sdsmt.cs492.assignment3.weatherapp.view;

import android.app.Fragment;
import android.view.LayoutInflater;
import android.widget.ImageView;
import android.view.View;
import android.view.ViewGroup;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.os.Bundle;
import android.widget.RelativeLayout;
import android.widget.TextView;
import edu.sdsmt.cs492.assignment3.weatherapp.R;
import edu.sdsmt.cs492.assignment3.weatherapp.model.Forecast;
import edu.sdsmt.cs492.assignment3.weatherapp.model.ForecastLocation;

/*********************************************************
*
*  Class that initializes all the textviews, layout, and
*  progress bar.
*	extends:
*		Fragment
*  @author Victor Dozal, Dan Halloran
********************************************************/
public class FragmentForecast extends Fragment
{
	public static final String LOCATION_KEY = "key_location";
	
	private TextView _textViewLocation;
	private TextView _textViewTemp;
	private TextView _textViewFeelsTemp;
	private TextView _textViewHumidity;
	private TextView _textViewChanceOfPrecip;
	private TextView _textViewTime;
	private TextView _textViewProgressBar;
	private ImageView _imageForecast;
	private RelativeLayout _layoutProgress;

	private ForecastLocation _forecastLocation;
	private Forecast _forecast;

	/************************************************************************
 	 * 	onCreate()
 	 * 	Inflates the fragment forecast and initializes the layout fields
 	 *  @author Victor Dozal, Dan Halloran
 	 * 	@param LayoutInflater inflater, ViewGroup container,
 	 * 		   Bundle savedInstanceState
 	 *  @return fragmentView
 	 ***********************************************************************/
	@Override
	public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
		// Inflate the fragment_forecast xml file
		View fragmentView = inflater.inflate(R.layout.fragment_forecast, null);
		// Initializes layout fields
		initializeLayoutFields(fragmentView);
		
		return fragmentView;
	}
	
	/************************************************************************
 	 * 	initializeLayoutFields()
 	 * 	initializes the layout fields in the view
 	 *  @author Victor Dozal, Dan Halloran
 	 * 	@param View fragmentView
 	 *  @return nothing
 	 ***********************************************************************/
	public void initializeLayoutFields(View fragmentView){
		_textViewProgressBar = (TextView) fragmentView.findViewById(R.id.textViewProgressBar);
		_imageForecast = (ImageView) fragmentView.findViewById(R.id.imageForecast);
		_textViewLocation = (TextView) fragmentView.findViewById(R.id.textViewLocation);
		_textViewTemp = (TextView) fragmentView.findViewById(R.id.textViewTemp);
		_layoutProgress = (RelativeLayout) fragmentView.findViewById(R.id.layoutProgress);
		_textViewChanceOfPrecip = (TextView) fragmentView.findViewById(R.id.textViewChanceOfPrecip);
		_textViewTime = (TextView) fragmentView.findViewById(R.id.textViewAsOfTime);
		_textViewFeelsTemp = (TextView) fragmentView.findViewById(R.id.textViewFeelsLikeTemp);
		_textViewHumidity = (TextView) fragmentView.findViewById(R.id.textViewHumidity);
	}

	/************************************************************************
 	 * 	getCorrectLoadBar()
 	 * 	displays the progress bar and message showing there's no connectivity
 	 *  @author Victor Dozal, Dan Halloran
 	 * 	@param Boolean showBar, String msg
 	 *  @return nothing
 	 ***********************************************************************/
	public void getCorrectLoadBar(Boolean showBar, String msg) {
		// If there is a layout progress
		if(!(_layoutProgress == null)){
			// If we don't want to show the bar, set it to invisible
			if(!showBar){
				_layoutProgress.setVisibility(View.INVISIBLE);
			}
			// Otherwise, set it to visible
			else{
				_layoutProgress.setVisibility(View.VISIBLE);
			}
		}
		
		// If the text progress bar is not null, set the text to the message
		if(_textViewProgressBar != null){
			_textViewProgressBar.setText(msg);			
		}	
	}
	
	/************************************************************************
 	 * 	updateWeather()
 	 * 	Fills all the text views with data if available otherwise "N/A".
 	 *  @author Victor Dozal, Dan Halloran
 	 * 	@param Forecast forecast
 	 *  @return nothing
 	 ***********************************************************************/
	public void updateWeather(Forecast forecast) {
		_forecast = forecast;
		
		// If the forecast is not empty
		if (_forecast != null) {
			// If any field is not null, then display its data
			// Otherwise, display "Not Available"
			if (_forecast._Temp == null){
				_textViewTemp.setText("N/A");
			}
			else{
				_textViewTemp.setText(_forecast._Temp + "�F");
			}

			if (_forecast._Humidity == null){
				_textViewHumidity.setText("N/A");
			}
			else{
				_textViewHumidity.setText(_forecast._Humidity + "%");
			}

			if (_forecast._PrecipChance == null){
				_textViewChanceOfPrecip.setText("N/A");
			}
			else{
				_textViewChanceOfPrecip.setText(_forecast._PrecipChance + "%");
			}
			
			if (_forecast._bitmap != null){
				Bitmap bitmap = BitmapFactory.decodeByteArray(_forecast._bitmap, 0, _forecast._bitmap.length);
				_imageForecast.setImageBitmap(bitmap);
			}

			if (_forecast._Feels == null){
				_textViewFeelsTemp.setText("N/A");
			}
			else{
				_textViewFeelsTemp.setText(_forecast._Feels + "�F");
			}

			if (_forecast._Time == null){
				_textViewTime.setText("N/A");
			}
			else{
				_textViewTime.setText(_forecast._Time);
			}
		} 
	}
	
	/************************************************************************
 	 * 	getLocation()
 	 * 	Displays location data if it's available.
 	 *  @author Victor Dozal, Dan Halloran
 	 * 	@param ForecastLocation forecastLocation
 	 *  @return nothing
 	 ***********************************************************************/
	public void getLocation(ForecastLocation forecastLocation) {
		_forecastLocation = forecastLocation;
		
		// If the forecast location is not null
		if (_forecastLocation != null) {
			String locationString = "";
			
			// If the city exists, then display data,
			// otherwise, display Not Available
			if (_forecastLocation._City != null){
				locationString += _forecastLocation._City;
				if (_forecastLocation._State != null){
					locationString += " " + _forecastLocation._State;
				}
			}

			_textViewLocation.setText(locationString);
		}
	}
}